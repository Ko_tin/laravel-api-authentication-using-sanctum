<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginUserRequest;
use App\Http\Requests\StoreUserRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Create user
     * 
     * @param App\Http\Requests\StoreUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createUser(StoreUserRequest $request)
    {
        // Validated request fields
        $validated = $request->validated();
        $validated['password'] = Hash::make($validated['password']);    // Hash user password

        // Create new user
        $user = User::create($validated);

        // Response JSON
        return jsonResponse([
            'token' => $user->createToken('API TOKEN')->plainTextToken,
        ], 'Created user successfully');
    }

    /**
     * Login user
     * 
     * @param App\Http\Requests\LoginUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginUser(LoginUserRequest $request)
    {
        // Validated request fields
        $validated = $request->validated();

        if (!Auth::attempt($validated)) {
            // Response JSON
            return jsonResponse(null, 'Incorrect password', 401);
        }

        $user = User::where('email', $validated['email'])->first();

        // Response JSON
        return jsonResponse([
            'token' => $user->createToken('API TOKEN')->plainTextToken
        ], 'User logged in successfully');
    }
}
