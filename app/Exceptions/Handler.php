<?php

namespace App\Exceptions;

use Throwable;
use Illuminate\Database\QueryException;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * Render an exception into an HTTP response
     */
    public function render($request, Throwable $exception)
    {
        // Form request validation exception
        if ($request->acceptsJson() && ($exception instanceof ValidationException) && !app()->hasDebugModeEnabled()) {
            // Response JSON
            return jsonResponse(null, 'Validation error', 422, $exception->errors());
        }

        // Database query exception
        if ($request->acceptsJson() && ($exception instanceof QueryException) && !app()->hasDebugModeEnabled()) {
            // Response JSON
            return jsonResponse(null, 'Internal server error', 500);
        }

        return parent::render($request, $exception);
    }
}
