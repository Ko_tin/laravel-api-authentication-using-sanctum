<?php

/**
 * AIP JSON response format
 */
if (!function_exists('jsonResponse')) {
    function jsonResponse(mixed $result = null, string $message = 'Success', int $statusCode = 200, mixed $errors = null) {
        $responseData = [
            'statusCode' => $statusCode,
            'results' => $result,
            'message' => $message
        ];

        if (!is_null($errors)) {
            $responseData['errors'] = $errors;
        }

        return response()->json($responseData, $statusCode);
    }
}